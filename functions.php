<?php
function find_order_by_id($id){
    global $conn;
    $sql = "SELECT * FROM reservations ";
    $sql .= "WHERE reservation_num='" . mysqli_real_escape_string($conn, $id) . "'";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    mysqli_free_result($result);
    return $row;
}

function getAllOrders() {
	global $conn;
	$sql = "SELECT * FROM orders ORDER BY pickup_time";
	$result = mysqli_query($conn, $sql);

    return $result;
}
?>