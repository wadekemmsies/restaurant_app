<?php require_once("connect.php");
require_once("functions.php");

$orders = getAllOrders();

?> 
<body>
<h1>Orders: </h1>
<a href="order.php">Place an Order</a>
<div class="row">

    <?php while($row = mysqli_fetch_assoc($orders)){?>
        <div class="col">
        <hr>
        <h3>Order #: <b><?php echo $row["order_id"]?></b></h3>
        
        <p style="color: red"> <b>- <?php echo $row['order_content']?></b></p>
        <p>PICKUP TIME: <b><?php echo $row['pickup_time']?></b></p>
        
        </div>
      
        
    <?php } ?>
    <hr> 
        </div>
    
<script>setTimeout(function() {
  location.reload();
}, 10000);</script>
</body>
</html>